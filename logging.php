<?php
namespace logging;

define("LOG_LEVEL_SILENCE", 0);
define("LOG_LEVEL_ERROR", 1);
define("LOG_LEVEL_WARNING", 2);
define("LOG_LEVEL_INFO", 3);
define("LOG_LEVEL_DEBUG", 4);

$LEVEL_MAPPING = array(
    1 => 'ERROR  ',
    2 => 'WARNING',
    3 => 'INFO   ',
    4 => 'DEBUG  ',
);

$LEVEL = LOG_LEVEL_INFO;

function _log_array($level, $format, $array)
{
    global $LEVEL, $LEVEL_MAPPING;
    if ($LEVEL < $level) {
        return;
    }
 
    $str = vsprintf($format, $array);
    if ($level < LOG_LEVEL_SILENCE) {
        $level = LOG_LEVEL_SILENCE;
    }
    if ($level > LOG_LEVEL_DEBUG) {
        $level = LOG_LEVEL_DEBUG;
    }
    $level_str = $LEVEL_MAPPING[$level];
    echo date('Y-m-d H:i:s') . '  ' . $level_str . '   ' . $str;
}

function log()
{
    $args = func_get_args();
    $level = array_shift($args);
    $format = array_shift($args);
    _log_array($level, $format, $args);
}

function error()
{
    $args = func_get_args();
    $format = array_shift($args);
    _log_array(LOG_LEVEL_ERROR, $format . "\n", $args);
}

function warning()
{
    $args = func_get_args();
    $format = array_shift($args);
    _log_array(LOG_LEVEL_WARNING, $format . "\n", $args);
}

function info()
{
    $args = func_get_args();
    $format = array_shift($args);
    _log_array(LOG_LEVEL_INFO, $format . "\n", $args);
}

function debug()
{
    $args = func_get_args();
    $format = array_shift($args);
    _log_array(LOG_LEVEL_DEBUG, $format . "\n", $args);
}
